# PlayToKodi
PlayToKodi is an Android app that allows you to play various media to your Kodi Media Center from your Smartphone or Tablet.
It communicates with Kodi via the JSON-RPC api built in Kodi.

### Supported Apps and their Kodi plugins
When using one of the supported apps or websites just tap the "Share" button and select "PlayToKodi", the app will check if you have the correct plugin installed on Kodi and starts playing the media.

PlayToKodi supports the following apps :

- YouTube (requires plugin installed on Kodi (plugin.video.youtube)).
- Vimeo (requires plugin installed on Kodi (plugin.video.vimeo)).
- Twitch.tv (requires plugin installed on Kodi (plugin.video.twitch)).
- DailyMotion (requires plugin installed on Kodi (plugin.video.dailymotion_com)).
- SoundCloud (requires plugin installed on Kodi (plugin.audio.soundcloud)).
- MyVideo.de (requires plugin installed on Kodi (plugin.video.myvideo_de)).
- Google Drive and Google Docs (requires plugin installed on Kodi (plugin.video.gdrive)).
- Google Music (requires plugin installed on Kodi (plugin.audio.googlemusic.exp)).

### Torrent streams
When you click torrent magnets or torrent streams, the app will also be able to play them as long as you have one of the torrent streaming plugins installed on Kodi.

PlayToKodi supports the following torrent plugins :

- Pulsar
- Quasar
- KMediaTorrent
- Torrenter
- XBMCTorrent

### Remote Control
The app has a built-in remote control for Kodi which will be launched right after any media file is successfully sent to Kodi.
It implements the basic remote control functions like seeking, subtitles plugin, volume control, play, pause, fastforward and backward, etc...

### Download

Latest Release : [PlayToKodi-alpha2-release.apk](/uploads/05d3fd2bc8d3ee82c2f9fb5d349adba7/PlayToKodi-alpha2-release.apk)


