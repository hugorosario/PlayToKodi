package com.hugorosario.playtokodi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hugorosario.playtokodi.helpers.Kodi;
import com.hugorosario.playtokodi.helpers.ParsedURI;

/**
 * Created by Hugo on 18/02/2016.
 */
public class IntentProcessor extends AppCompatActivity {
    TextView txt;
    String log;
    boolean isWorking = false;
    String StreamURL = "";
    SharedPreferences prefs;
    Kodi kodi;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_streamprocessor);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        kodi = new Kodi(this);
        txt = (TextView)findViewById(R.id.status);
        pb = (ProgressBar)findViewById(R.id.progressBar);
        pb.setVisibility(View.VISIBLE);
        Intent intent = getIntent();
        String action = intent.getAction();
        if (Intent.ACTION_SEND.equals(action)) {
            String type = intent.getType();
            if (type!=null && type.contains("text/")) {
                StreamURL = intent.getStringExtra(Intent.EXTRA_TEXT);
                isWorking=true;
                initKodiStream();
            }
        }else
        if (Intent.ACTION_VIEW.equals(action)) {
            Uri data = intent.getData();
            if ((data != null) && (data.getScheme().equals("magnet") || data.getScheme().equals("playback") || data.toString().endsWith(".torrent"))) {
                StreamURL = data.toString();
                isWorking=true;
                initTorrentStream();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (!isWorking)
            super.onBackPressed();
    }

    public void initTorrentStream(){
        pb.setVisibility(View.VISIBLE);
        log("Processing torrent...");
        ParsedURI kuri = kodi.getKodiURI(StreamURL);
        if (kuri.success){
            log("Pushing torrent to Kodi '"+kuri.plugin+"'...");
            kodi.openStream(kuri.uri, new Kodi.OnRequestCompleteListener() {
                @Override
                public void OnRequestComplete(boolean success, String response) {
                    if (success) {
                        log("Done.");
                        Intent intent = new Intent(IntentProcessor.this, RemoteControl.class);
                        startActivity(intent);
                        ActivityCompat.finishAfterTransition(IntentProcessor.this);
                    } else
                        log("Error loading stream in Kodi!\n" + response);
                    pb.setVisibility(View.GONE);
                    isWorking=false;
                }
            });
        }else{
            if (kuri.errormessage.equals("NO_PLUGIN_SELECTED"))
                log("You have no torrent plugin selected!\nMake sure you select a plugin in the settings and it is correctly installed on Kodi!");
            pb.setVisibility(View.GONE);
            isWorking=false;
        }
    }

    public void initKodiStream(){
        pb.setVisibility(View.VISIBLE);
        ParsedURI kuri = kodi.getKodiURI(StreamURL);
        if (kuri.success) {
            StreamURL = kuri.uri;
            log("Pushing stream to Kodi '" + kuri.plugin + "'...");
        }else
            log("Pushing stream to Kodi...");
        kodi.openStream(StreamURL, new Kodi.OnRequestCompleteListener() {
            @Override
            public void OnRequestComplete(final boolean success, final String response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (success) {
                            log("Done.");
                            Intent intent = new Intent(IntentProcessor.this, RemoteControl.class);
                            startActivity(intent);
                            ActivityCompat.finishAfterTransition(IntentProcessor.this);
                        } else
                            log("Error loading stream in Kodi!\n" + response);
                        pb.setVisibility(View.GONE);
                        isWorking=false;
                    }
                });
            }
        });
    }

    public void log(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                log = txt.getText() + "\n" + log;
                txt.setText(msg);
            }
        });
    }

}
