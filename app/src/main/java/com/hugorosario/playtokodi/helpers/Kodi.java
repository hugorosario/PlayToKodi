package com.hugorosario.playtokodi.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Hugo Rosário on 30/01/2016.
 */
public class Kodi {
    public static final int WINDOW_DIALOG_KEYBOARD = 10103;
    public static final int WINDOW_FULLSCREEN_VIDEO = 12005;

    String Host;
    String Port;
    String magnetPluginSelected = "0";
    OnPlayerStatusListener PlayerStatusListener;
    Thread PlayerWatcher;
    int ActivePlayer = 1;

    public Kodi(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Host = prefs.getString("kodi_host","192.168.1.154");
        Port = prefs.getString("kodi_port","8081");
        magnetPluginSelected = prefs.getString("magnet_plugin","0");
    }

    public interface OnPlayerStatusListener{
        void onStatus(Status status);
    }

    public void setPlayerStatusListener(OnPlayerStatusListener playerStatusListener) {
        PlayerStatusListener = playerStatusListener;
        startPlayerWatcher();
    }

    public void unsetPlayerStatusListener() {
        PlayerStatusListener = null;
        stopPlayerWatcher();
    }

    private void stopPlayerWatcher() {
        if (PlayerWatcher!=null)
            PlayerWatcher.interrupt();
    }

    private void startPlayerWatcher(){
        stopPlayerWatcher();
        PlayerWatcher = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    Status status = new Status();
                    status.connected = false;
                    status.mediaPlaying = false;
                    Gson gson = new Gson();
                    String plresult = SendRequest("Player.GetActivePlayers","", true);
                    ActivePlayersResponse pl = gson.fromJson(plresult,ActivePlayersResponse.class);
                    if ((pl!=null) && (pl.result!=null)) {
                        status.connected = true;
                        if (pl.result.size()>0) {
                            ActivePlayer = pl.result.get(0).playerid;
                            status.mediaPlaying = true;
                        }
                        else
                            ActivePlayer = 1;
                    }
                    else
                        ActivePlayer = 1;
                    String result = SendRequest("Player.GetItem","\"playerid\":"+String.valueOf(ActivePlayer), true);
                    PlayerGetItemResponse rs = gson.fromJson(result, PlayerGetItemResponse.class);
                    if (rs!=null && rs.result!=null && rs.result.item!=null){
                        status.title = rs.result.item.label;
                        String result2 = SendRequest("Player.GetProperties","\"playerid\":"+String.valueOf(ActivePlayer)+",\"properties\":[\"time\",\"speed\",\"totaltime\",\"percentage\"]", true);
                        PlayerGetPropertiesResponse rs2 = gson.fromJson(result2, PlayerGetPropertiesResponse.class);
                        if (rs2!=null && rs2.result!=null) {
                            status.properties = rs2.result;
                            String result3 = SendRequest("GUI.GetProperties", "\"properties\":[\"currentwindow\"]", true);
                            GUIGetPropertiesResponse rs3 = gson.fromJson(result3, GUIGetPropertiesResponse.class);
                            if (rs3 != null && rs3.result != null && rs3.result.currentwindow != null) {
                                status.currentWindow = rs3.result.currentwindow.id;
                                status.playerFullscreen = (rs3.result.currentwindow.id == WINDOW_FULLSCREEN_VIDEO);
                            } else{
                                status.currentWindow = 0;
                                status.playerFullscreen = false;
                            }

                            if (PlayerStatusListener != null) {
                                PlayerStatusListener.onStatus(status);
                            }
                        }else{
                            if (PlayerStatusListener != null) {
                                PlayerStatusListener.onStatus(status);
                            }
                        }
                    }else {
                        if (PlayerStatusListener != null) {
                            PlayerStatusListener.onStatus(status);
                        }
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        PlayerWatcher.start();
    }


    public void openStream(final String streamUrl, final OnRequestCompleteListener listener){
        runTask(new OnRequestCompleteListener() {
            @Override
            public void OnRequestComplete(boolean success, String response) {
                try {
                    final URI uri = new URI(streamUrl);
                    if (uri.getScheme().equals("plugin")) {
                        checkPluginInstalled(uri.getHost(), new OnCheckPluginListener() {
                            @Override
                            public void OnCheckPlugin(boolean enabled) {
                                if (enabled)
                                    runTask(listener, true, "Player.Open", "\"item\":{\"file\":\"" + streamUrl + "\"}");
                                else
                                    listener.OnRequestComplete(false, "Plugin '" + uri.getHost() + "' is not installed!");
                            }
                        });
                    } else {
                        runTask(listener, true, "Player.Open", "\"item\":{\"file\":\"" + streamUrl + "\"}");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    runTask(listener, true, "Player.Open", "\"item\":{\"file\":\"" + streamUrl + "\"}");
                }
            }
        }, true, "Player.Stop", "\"playerid\":" + String.valueOf(ActivePlayer));
    }

    public void openChannel(final String streamUrl, final OnRequestCompleteListener listener){
        runTask(new OnRequestCompleteListener() {
            @Override
            public void OnRequestComplete(boolean success, String response) {
                    runTask(listener, false, "Player.Open", "\"item\":{\"file\":\"" + streamUrl + "\"}");
            }
        }, true, "Player.Stop", "\"playerid\":" + String.valueOf(ActivePlayer));
    }

    public void openSubtitlePlugin(){
        runTask(null, false, "GUI.ActivateWindow", "\"window\":\"subtitlesearch\"");
    }

    public void keyUp(){
        runTask(null, false, "Input.ExecuteAction", "\"action\":\"up\"");
    }

    public void keyDown(){
        runTask(null, false, "Input.ExecuteAction", "\"action\":\"down\"");
    }

    public void keyLeft(){
        runTask(null, false, "Input.ExecuteAction", "\"action\":\"left\"");
    }

    public void keyRight(){
        runTask(null, false,"Input.ExecuteAction","\"action\":\"right\"");
    }

    public void keySelect(){
        runTask(null, false, "Input.ExecuteAction", "\"action\":\"select\"");
    }

    public void keyShowOSD(){
        runTask(null, false, "Input.ShowOSD", "");
    }

    public void keyBack(){
        runTask(null, false, "Input.Back", "");
    }

    public void keyInfo(){
        runTask(null, false, "Input.Info", "");
    }

    public void keyContextMenu(){
        runTask(null, false, "Input.ContextMenu", "");
    }

    public void keyPrevious(){
        runTask(null, false, "Player.Goto", "\"playerid\":" + String.valueOf(ActivePlayer) + ",\"to\":\"previous\"");
    }

    public void keyNext(){
        runTask(null, false, "Player.Goto", "\"playerid\":" + String.valueOf(ActivePlayer) + ",\"to\":\"next\"");
    }

    public void keySmallForward(){
        runTask(null, false, "Player.Seek", "\"playerid\":" + String.valueOf(ActivePlayer) + ",\"value\":\"smallforward\"");
    }

    public void keySmallBackward(){
        runTask(null, false, "Player.Seek", "\"playerid\":" + String.valueOf(ActivePlayer) + ",\"value\":\"smallbackward\"");
    }

    public void keyBigForward(){
        runTask(null, false, "Player.Seek", "\"playerid\":" + String.valueOf(ActivePlayer) + ",\"value\":\"bigforward\"");
    }

    public void keyBigBackward(){
        runTask(null, false, "Player.Seek", "\"playerid\":" + String.valueOf(ActivePlayer) + ",\"value\":\"bigbackward\"");
    }

    public void keySeek(int percentage){
        runTask(null, false, "Player.Seek", "\"playerid\":" + String.valueOf(ActivePlayer) + ",\"value\":" + String.valueOf(percentage));
    }

    public void keyStop(){
        runTask(null, false, "Player.Stop", "\"playerid\":" + String.valueOf(ActivePlayer));
    }

    public void keyPlayPause(){
        runTask(null, false, "Player.PlayPause", "\"playerid\":" + String.valueOf(ActivePlayer));
    }

    public void keyVolumeUp(){
        runTask(null, false, "Application.SetVolume", "\"volume\":\"increment\"");
    }

    public void keyVolumeDown(){
        runTask(null, false, "Application.SetVolume", "\"volume\":\"decrement\"");
    }

    public void keyVolumeMute(){
        runTask(null, false, "Application.SetMute", "\"mute\":\"toggle\"");
    }

    public void keyIncrementSpeed(){
        runTask(null, false, "Player.SetSpeed", "\"playerid\":" + String.valueOf(ActivePlayer) + ",\"speed\":\"increment\"");
    }

    public void keyDecrementSpeed(){
        runTask(null, false, "Player.SetSpeed", "\"playerid\":" + String.valueOf(ActivePlayer) + ",\"speed\":\"decrement\"");
    }

    public void sendText(String text){
        runTask(null, false,"Input.SendText", "\"text\":\"" + text + "\",\"done\":true");
    }

    public interface OnRequestCompleteListener{
        void OnRequestComplete(boolean success, String response);
    }

    public interface OnCheckPluginListener{
        void OnCheckPlugin(boolean enabled);
    }

    public void checkPluginInstalled(final String plugin, final OnCheckPluginListener listener){
        new Thread(new Runnable() {
            @Override
            public void run() {
                String result = SendRequest("Addons.GetAddons","", true);
                Gson gson = new Gson();
                GetAddonsResponse rs = gson.fromJson(result, GetAddonsResponse.class);
                boolean found = false;
                if ((rs!=null) && (rs.result!=null) && (rs.result.addons!=null)){
                    for (GetAddonsResponse.GetAddonsResult.AddonDetail addon : rs.result.addons){
                        if (addon.addonid.equals(plugin)) {
                            found = true;
                            break;
                        }
                    }
                }
                listener.OnCheckPlugin(found);
            }
        }).start();
    }

    public void runTask(OnRequestCompleteListener listener, boolean wait, String method, String params){
        if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) && (wait)) {
            new SendRequestTask(listener,wait).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, method, params);
        } else {
            new SendRequestTask(listener,wait).execute(method, params);
        }
    }

    private class SendRequestTask extends AsyncTask<String, Void, String> {
        OnRequestCompleteListener listener;
        boolean wait = false;

        public SendRequestTask(OnRequestCompleteListener listener, boolean wait) {
            this.listener = listener;
            this.wait = wait;
        }

        @Override
        protected String doInBackground(String... params) {
                return SendRequest(params[0], params[1], wait);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            if (listener!=null){
                if (result.isEmpty())
                    listener.OnRequestComplete(false, result);
                else
                    listener.OnRequestComplete(true, result);
            }
        }
    }

    private String SendRequest(String method, String params, boolean wait){
        String response = "";
        try {
            URL url = new URL("http://"+Host+":"+Port+"/jsonrpc");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", "application/json");
            if (!wait)
                conn.setReadTimeout(1);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            if (wait)
                conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            String cmd = "{\"jsonrpc\":\"2.0\", \"id\":1, \"method\": \"" + method + "\", \"params\":{" + params + "}}";
            writer.write(cmd);
            writer.flush();
            writer.close();
            os.close();
            int responseCode=conn.getResponseCode();
            if (wait){                
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (!wait)
                response="{}";
            else
                response = "";
        }
        if (!wait)
            response="{}";
        return response;
    }

    public ParsedURI getKodiURI(String url){
        ParsedURI result = new ParsedURI();
        String link = url;
        String str = null;
        try {
            Matcher r2;
            Matcher r1;
            link = url.replace("playback://","");
            URI uri = new URI(link);
            if (uri.getScheme().equals("magnet") || (uri.toString().endsWith(".torrent"))) {
                if (magnetPluginSelected.equals("0")) {
                    result.errormessage = "NO_PLUGIN_SELECTED";
                } else {
                    String params = "";
                    switch (magnetPluginSelected){
                        case "plugin.video.pulsar" : {params = "/play?uri=";}break;
                        case "plugin.video.quasar" : {params = "/play?uri=";}break;
                        case "plugin.video.kmediatorrent" : {params = "/play/";}break;
                        case "plugin.video.torrenter" : {params = "/?action=playSTRM&url=";}break;
                        case "plugin.video.xbmctorrent" : {params = "/play/";}break;
                    }
                    str = "plugin://" + magnetPluginSelected + params + Uri.encode(uri.toString(), "UTF-8");
                    result.plugin = magnetPluginSelected;
                }
            } else if (uri.getHost().endsWith("youtube.com") || uri.getHost().endsWith("youtu.be")) {

                r2 = Pattern.compile(".*v=([a-z0-9_\\-]+)(?:&.)*.*", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                if (r2.matches()) {
                    str = "plugin://plugin.video.youtube/play/?video_id=" + r2.group(1);
                    result.plugin = "plugin.video.youtube";
                }
                if (str == null) {
                    r2 = Pattern.compile(".*v%3D([a-z0-9_\\-]+)(?:&.)*.*", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                    if (r2.matches()) {
                        str = "plugin://plugin.video.youtube/play/?video_id=" + r2.group(1);
                        result.plugin = "plugin.video.youtube";
                    }
                }
                if (str == null) {
                    r2 = Pattern.compile(".*embed/([a-z0-9_\\-]+)(?:&.)*.*", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                    if (r2.matches()) {
                        str = "plugin://plugin.video.youtube/play/?video_id=" + r2.group(1);
                        result.plugin = "plugin.video.youtube";
                    }
                }
                if (str == null) {
                    r2 = Pattern.compile(".*list=([a-z0-9_\\-]+)(?:&.)*.*", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                    if (r2.matches()) {
                        str = "plugin://plugin.video.youtube/play/?playlist_id=" + r2.group(1) + "&order=default";
                        result.plugin = "plugin.video.youtube";
                    }
                }
                if (str == null) {
                    r2 = Pattern.compile(".*/([A-Za-z0-9_\\-]+)", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                    if (r2.matches()) {
                        str = "plugin://plugin.video.youtube/play/?video_id=" + r2.group(1);
                        result.plugin = "plugin.video.youtube";
                    }
                }
                if (str == null) {
                    r2 = Pattern.compile(".*video_ids=([A-Za-z0-9_\\-]+)*.*", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                    if (r2.matches()) {
                        str = "plugin://plugin.video.youtube/play/?video_id=" + r2.group(1);
                        result.plugin = "plugin.video.youtube";
                    }
                }
                if (str == null) {
                    r2 = Pattern.compile(".*video_ids%3D([A-Za-z0-9_\\-]+)*.*", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                    if (r2.matches()) {
                        str = "plugin://plugin.video.youtube/play/?video_id=" + r2.group(1);
                        result.plugin = "plugin.video.youtube";
                    }
                }
                if (str==null || str.isEmpty()) {
                    str = uri.toString();
                }
            } else if (uri.getHost().endsWith("twitch.tv")) {
                r2 = Pattern.compile(".*/([A-Za-z0-9_\\-]+)/mobile", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                if (r2.matches()) {
                    str = "plugin://plugin.video.twitch/playLive/" + r2.group(1) + "/";
                    result.plugin = "plugin.video.twitch";
                }
                if (str == null) {
                    r2 = Pattern.compile(".*/([A-za-z0-9])/([A-Za-z0-9_\\-]+)", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                    if (r2.matches()) {
                        str = "plugin://plugin.video.twitch/playVideo/" + r2.group(1) + r2.group(2) + "/";
                        result.plugin = "plugin.video.twitch";
                    }
                }
                if (str == null) {
                    r2 = Pattern.compile(".*/([A-Za-z0-9_\\-]+)", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                    if (r2.matches()) {
                        str = "plugin://plugin.video.twitch/playLive/" + r2.group(1) + "/";
                        result.plugin = "plugin.video.twitch";
                    }
                }
                if (str==null || str.isEmpty()) {
                    str = uri.toString();
                }
            } else if (uri.getHost().endsWith("vimeo.com")) {
                r1 = Pattern.compile(".*/([0-9]+)", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                str = r1.matches() ? "plugin://plugin.video.vimeo/play/?video_id=" + r1.group(1) : uri.toString();
                result.plugin = "plugin.video.vimeo";
            } else if (uri.getHost().endsWith("dailymotion.com")) {
                r1 = Pattern.compile(".*/video/(.*)", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                if (r1.matches()) {
                    str = "plugin://plugin.video.dailymotion_com/?mode=playVideo&url=" + r1.group(1);
                    result.plugin = "plugin.video.dailymotion_com";
                } else {
                    str = uri.toString();
                }
            } else if (uri.getHost().endsWith("soundcloud.com")) {
                str = "plugin://plugin.audio.soundcloud/play/?url=" + Uri.encode(uri.toString());
                result.plugin = "plugin.audio.soundcloud";
            } else if (uri.getHost().endsWith("myvideo.de")) {
                r1 = Pattern.compile(".*watch/([0-9]+)/.*", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                if (r1.matches()) {
                    str = "plugin://plugin.video.myvideo_de/video/" + r1.group(1) + "/play";
                    result.plugin = "plugin.video.myvideo_de";
                } else {
                    str = uri.toString();
                }
            } else if (uri.getHost().endsWith("drive.google.com") || uri.getHost().endsWith("docs.google.com")) {
                r1 = Pattern.compile(".*/([a-z]+)/d/([a-z0-9_\\-]+)/.*", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                if (r1.matches()) {
                    str = "plugin://plugin.video.gdrive?mode=streamURL&url=https://docs.google.com/" + r1.group(1) + "/d/" + r1.group(2) + "/preview";
                    result.plugin = "plugin.video.gdrive";
                } else {
                    str = uri.toString();
                }
            } else if (uri.getHost().endsWith("play.google.com")) {
                r2 = Pattern.compile(".*/music/.*/(T.*)", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                if (r2.matches()) {
                    str = "plugin://plugin.audio.googlemusic.exp/?action=play_song&song_id=" + r2.group(1);
                    result.plugin = "plugin.audio.googlemusic.exp";
                }
                if (str == null) {
                    r2 = Pattern.compile(".*/music/.*/(B.*)", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                    if (r2.matches()) {
                        str = "plugin://plugin.audio.googlemusic.exp/?action=play_all&album_id=" + r2.group(1);
                        result.plugin = "plugin.audio.googlemusic.exp";
                    }
                }
                if (str == null) {
                    r2 = Pattern.compile(".*/music/.*/(AM.*)", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                    if (r2.matches()) {
                        str = "plugin://plugin.audio.googlemusic.exp/?action=play_all&share_token=" + r2.group(1);
                        result.plugin = "plugin.audio.googlemusic.exp";
                    }
                }
                if (str == null) {
                    r2 = Pattern.compile(".*/music/.*/(A.*)", Pattern.CASE_INSENSITIVE).matcher(uri.toString());
                    if (r2.matches()) {
                        str = "plugin://plugin.audio.googlemusic.exp/?action=play_all&artist_id=" + r2.group(1);
                        result.plugin = "plugin.audio.googlemusic.exp";
                    }
                }
                if (str == null) {
                    str = uri.toString();
                }
            } else {
                str = uri.toString();
            }
        } catch (Exception e) {
            str = url;
        }
        result.uri = str;
        result.success = !(url.equals(str) && url!=null && url != "");
        return result;
    }

    public static class JsonRpcResponse {
        int id;
        String jsonrpc;
        JsonRpcError error;
    }


    public static class JsonRpcError{
        int code;
        String message;
    }

    public static class GetAddonsResponse extends JsonRpcResponse {
        GetAddonsResult result;

        public static class GetAddonsResult {
            ArrayList<AddonDetail> addons;

            public static class AddonDetail {
                String addonid;
                String type;
            }
        }
    }

    public static class PlayerGetItemResponse extends JsonRpcResponse {
        GetItemResult result;

        public static class GetItemResult {
            Item item;

            public static class Item {
                String label;
                String type;
            }
        }
    }

    public static class PlayerGetPropertiesResponse extends JsonRpcResponse {
        public GetPropertiesResult result;

        public static class GetPropertiesResult {
            public int speed;
            public double percentage;
            public TimeItem time;
            public TimeItem totaltime;

            public static class TimeItem {
                public int hours;
                public int milliseconds;
                public int minutes;
                public int seconds;
            }
        }
    }

    public static class GUIGetPropertiesResponse extends JsonRpcResponse {
        GUIGetPropertiesResult result;

        public static class GUIGetPropertiesResult {
            CurrentWindow currentwindow;

            public static class CurrentWindow {
                int id;
                String label;
            }
        }
    }

    public static class ActivePlayersResponse extends JsonRpcResponse {
        ArrayList<Player> result;

        public static class Player {
            int playerid;
            String type;
        }
    }

    public static class Status {
        public String title;
        public PlayerGetPropertiesResponse.GetPropertiesResult properties;
        public int currentWindow;
        public boolean playerFullscreen;
        public boolean connected;
        public boolean mediaPlaying;
    }
}
