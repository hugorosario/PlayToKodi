package com.hugorosario.playtokodi.helpers;

/**
 * Created by Hugo Rosário on 17/02/2016.
 */
public class ParsedURI {
    public String uri;
    public String errormessage;
    public String plugin;
    public boolean success;
}
