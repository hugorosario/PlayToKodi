package com.hugorosario.playtokodi.helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hugorosario.playtokodi.R;

import java.util.ArrayList;

/**
 * Created by Hugo Rosário on 04/09/2016.
 */
public class SearchResultAdapter extends BaseAdapter {
    ArrayList<SearchResult> results;
    Context mContext;
    public SearchResultAdapter(ArrayList<SearchResult> results, Context context) {
        this.results = results;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return this.results.size();
    }

    @Override
    public SearchResult getItem(int i) {
        return this.results.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        SearchResult item = results.get(i);
        View v = view;
        if (v==null)
            v = ((LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.result_item, null);
        ((TextView)v.findViewById(R.id.Name)).setText(item.getName());
        ((TextView)v.findViewById(R.id.Size)).setText(item.getSize()+ " S:"+item.getSeeders()+"/P:"+item.getLeechers());

        return v;
    }
}
