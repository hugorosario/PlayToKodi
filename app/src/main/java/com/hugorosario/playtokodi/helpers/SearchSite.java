package com.hugorosario.playtokodi.helpers;

/**
 * Created by Hugo Rosário on 04/09/2016.
 */
public class SearchSite {
    private final int id;
    private final String key;
    private final String name;
    private final String rssFeedUrl;
    private final boolean isPrivate;

    public SearchSite(int id, String key, String name, String rssFeedUrl, boolean isPrivate) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.rssFeedUrl = rssFeedUrl;
        this.isPrivate = isPrivate;
    }

    public int getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getRssFeedUrl() {
        return rssFeedUrl;
    }

    public String getBaseUrl() {
        return rssFeedUrl;
    }

    public boolean isPrivate() {
        return isPrivate;
    }
}
