package com.hugorosario.playtokodi.helpers;

import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.hugorosario.playtokodi.R;

/**
 * Created by Hugo on 19/02/2016.
 */
public class RepeatTouchListener implements View.OnTouchListener {

    public interface MyClickListener{
        void OnClick();
    }

    private Handler mHandler;
    private MyClickListener clickListener;
    private boolean mRepeat = false;
    private int currentDelay = 100;

    Runnable mAction = new Runnable() {
        @Override public void run() {
            if (clickListener!=null)
                clickListener.OnClick();

            currentDelay -= 75;
            if (currentDelay < 20) currentDelay = 20;
            if (mRepeat)
                mHandler.postDelayed(this, currentDelay);
        }
    };

    public RepeatTouchListener(boolean repeat, MyClickListener clickListener) {
        this.clickListener = clickListener;
        this.mRepeat = repeat;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                v.startAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.button_click));
                if (mHandler != null) return true;
                mHandler = new Handler();
                currentDelay = 250;
                mHandler.postDelayed(mAction, 0);
                break;
            case MotionEvent.ACTION_UP:
                v.startAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.button_release));
                if (mHandler == null) return true;
                mHandler.removeCallbacks(mAction);
                mHandler = null;
                break;
            case MotionEvent.ACTION_CANCEL:
                v.startAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.button_release));
                if (mHandler == null) return true;
                mHandler.removeCallbacks(mAction);
                mHandler = null;
                break;
        }
        return true;
    }
}
