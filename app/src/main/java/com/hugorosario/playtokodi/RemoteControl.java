package com.hugorosario.playtokodi;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.hugorosario.playtokodi.helpers.Kodi;
import com.hugorosario.playtokodi.helpers.RepeatTouchListener;

public class RemoteControl extends AppCompatActivity {
    ImageView btLeft;
    ImageView btRight;
    ImageView btUp;
    ImageView btDown;
    ImageView btBack;
    ImageView btContext;
    ImageView btInfo;
    ImageView btSelect;
    ImageView btSubtitle;
    ImageView btPrev;
    ImageView btNext;
    ImageView btBackward;
    ImageView btForward;
    ImageView btStop;
    ImageView btPlayPause;
    ImageView btVolUp;
    ImageView btVolDown;
    ImageView btVolMute;
    SeekBar seekBar;
    private boolean mediaPlaying = false;
    private boolean showingOSD = false;
    private boolean isInputText = false;

    Kodi kodi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_control);
        btLeft = (ImageView)findViewById(R.id.arrow_left);
        btRight = (ImageView)findViewById(R.id.arrow_right);
        btUp = (ImageView)findViewById(R.id.arrow_up);
        btDown = (ImageView)findViewById(R.id.arrow_down);
        btBack = (ImageView)findViewById(R.id.back);
        btContext = (ImageView)findViewById(R.id.context);
        btInfo = (ImageView)findViewById(R.id.info);
        btSelect = (ImageView)findViewById(R.id.select);
        btSubtitle = (ImageView)findViewById(R.id.subtitles);
        btPrev = (ImageView)findViewById(R.id.previous);
        btNext = (ImageView)findViewById(R.id.next);
        btBackward = (ImageView)findViewById(R.id.backward);
        btForward = (ImageView)findViewById(R.id.forward);
        btStop = (ImageView)findViewById(R.id.stop);
        btPlayPause = (ImageView)findViewById(R.id.play);
        btVolDown = (ImageView)findViewById(R.id.vol_down);
        btVolMute = (ImageView)findViewById(R.id.vol_mute);
        btVolUp = (ImageView)findViewById(R.id.vol_up);
        seekBar = (SeekBar)findViewById(R.id.seekBar);
        setKodi();

        btLeft.setOnTouchListener(new RepeatTouchListener(true, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                if (mediaPlaying && !showingOSD)
                    kodi.keySmallBackward();
                else
                    kodi.keyLeft();
            }
        }));

        btRight.setOnTouchListener(new RepeatTouchListener(true, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                if (mediaPlaying && !showingOSD)
                    kodi.keySmallForward();
                else
                    kodi.keyRight();
            }
        }));

        btUp.setOnTouchListener(new RepeatTouchListener(true, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                if (mediaPlaying && !showingOSD)
                    kodi.keyBigForward();
                else
                    kodi.keyUp();
            }
        }));

        btDown.setOnTouchListener(new RepeatTouchListener(true, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                if (mediaPlaying && !showingOSD)
                    kodi.keyBigBackward();
                else
                    kodi.keyDown();
            }
        }));

        btBack.setOnTouchListener(new RepeatTouchListener(true, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                kodi.keyBack();
            }
        }));

        btContext.setOnTouchListener(new RepeatTouchListener(false, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                kodi.keyContextMenu();
            }
        }));

        btInfo.setOnTouchListener(new RepeatTouchListener(false, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                kodi.keyInfo();
            }
        }));

        btSelect.setOnTouchListener(new RepeatTouchListener(false, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                if (mediaPlaying && !showingOSD)
                    kodi.keyShowOSD();
                else
                    kodi.keySelect();
            }
        }));

        btSubtitle.setOnTouchListener(new RepeatTouchListener(false, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                kodi.openSubtitlePlugin();
            }
        }));

        btPrev.setOnTouchListener(new RepeatTouchListener(false, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                kodi.keyPrevious();
            }
        }));

        btNext.setOnTouchListener(new RepeatTouchListener(false, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                kodi.keyNext();
            }
        }));

        btForward.setOnTouchListener(new RepeatTouchListener(false, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                kodi.keyIncrementSpeed();
            }
        }));

        btBackward.setOnTouchListener(new RepeatTouchListener(false, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                kodi.keyDecrementSpeed();
            }
        }));

        btStop.setOnTouchListener(new RepeatTouchListener(false, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                kodi.keyStop();
            }
        }));


        btPlayPause.setOnTouchListener(new RepeatTouchListener(false, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                kodi.keyPlayPause();
            }
        }));

        btVolUp.setOnTouchListener(new RepeatTouchListener(true, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                kodi.keyVolumeUp();
            }
        }));

        btVolDown.setOnTouchListener(new RepeatTouchListener(true, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                kodi.keyVolumeDown();
            }
        }));

        btVolMute.setOnTouchListener(new RepeatTouchListener(false, new RepeatTouchListener.MyClickListener() {
            @Override
            public void OnClick() {
                kodi.keyVolumeMute();
            }
        }));

    }

    public void setKodi(){
        if (kodi!=null)
            kodi.unsetPlayerStatusListener();
        kodi = new Kodi(this);
        ((TextView) findViewById(R.id.stopMessage)).setText(R.string.connectingKodi);
        kodi.setPlayerStatusListener(new Kodi.OnPlayerStatusListener() {
            @Override
            public void onStatus(final Kodi.Status status) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (status!=null && status.connected){
                            mediaPlaying = status.mediaPlaying;
                            showingOSD = !status.playerFullscreen;
                            if (status.currentWindow != Kodi.WINDOW_DIALOG_KEYBOARD)
                                isInputText = false;
                            if (status.currentWindow == Kodi.WINDOW_DIALOG_KEYBOARD && !isInputText){
                                isInputText = true;
                                AlertDialog.Builder alert = new AlertDialog.Builder(RemoteControl.this);
                                final EditText edittext = new EditText(RemoteControl.this);
                                edittext.requestFocus();
                                alert.setMessage(null);
                                alert.setTitle(R.string.enterText);
                                alert.setView(edittext);

                                alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        String txt = edittext.getText().toString();
                                        kodi.sendText(txt);
                                        dialog.dismiss();
                                    }
                                });

                                alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();
                                    }
                                });


                                AlertDialog dialog = alert.create();
                                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                                dialog.show();
                            }
                            if (mediaPlaying) {
                                seekBar.setMax(100);
                                seekBar.setProgress((int) status.properties.percentage);
                                seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                    @Override
                                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                        if (fromUser) {
                                            kodi.keySeek(progress);
                                        }
                                    }

                                    @Override
                                    public void onStartTrackingTouch(SeekBar seekBar) {

                                    }

                                    @Override
                                    public void onStopTrackingTouch(SeekBar seekBar) {

                                    }
                                });
                                findViewById(R.id.mediacontrols).setVisibility(View.VISIBLE);
                                findViewById(R.id.stopStatus).setVisibility(View.INVISIBLE);
                                ((TextView) findViewById(R.id.mediatitle)).setText(status.title);
                                ((TextView) findViewById(R.id.time)).setText(String.format("%02d", status.properties.time.hours) + ":" + String.format("%02d", status.properties.time.minutes) + ":" + String.format("%02d", status.properties.time.seconds));
                                ((TextView) findViewById(R.id.totaltime)).setText(String.format("%02d", status.properties.totaltime.hours) + ":" + String.format("%02d", status.properties.totaltime.minutes) + ":" + String.format("%02d", status.properties.totaltime.seconds));
                                if (status.properties.speed == 0)
                                    btPlayPause.setImageResource(R.drawable.play);
                                if (status.properties.speed > 0)
                                    btPlayPause.setImageResource(R.drawable.pause);
                            }else{
                                findViewById(R.id.mediacontrols).setVisibility(View.INVISIBLE);
                                findViewById(R.id.stopStatus).setVisibility(View.VISIBLE);
                                ((TextView) findViewById(R.id.stopMessage)).setText(R.string.noMediaLoaded);
                                mediaPlaying=false;
                                showingOSD = true;
                            }
                        }else{
                            findViewById(R.id.mediacontrols).setVisibility(View.INVISIBLE);
                            findViewById(R.id.stopStatus).setVisibility(View.VISIBLE);
                            ((TextView) findViewById(R.id.stopMessage)).setText(R.string.unableToConnectKodi);
                            mediaPlaying=false;
                            showingOSD = true;
                         }
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 5000)
            setKodi();
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
         setKodi();
         super.onResume();
    }

    @Override
    protected void onPause() {
        kodi.unsetPlayerStatusListener();
        super.onPause();
    }
}
