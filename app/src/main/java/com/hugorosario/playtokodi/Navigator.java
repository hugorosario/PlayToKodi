package com.hugorosario.playtokodi;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.CursorLoader;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.hugorosario.playtokodi.helpers.Kodi;
import com.hugorosario.playtokodi.helpers.SearchHelper;
import com.hugorosario.playtokodi.helpers.SearchResult;
import com.hugorosario.playtokodi.helpers.SearchResultAdapter;
import com.hugorosario.playtokodi.helpers.SearchSite;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Navigator extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    ProgressBar pb;
    WebView wv;
    LinearLayout searchTorrent;
    Kodi kodi;
    Dialog waitDialog;
    DrawerLayout drawer;
    SearchHelper searchHelper;
    ListView searchResults;
    SearchResultAdapter searchAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stream_caster);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setCheckedItem(R.id.nav_home);
        navigationView.setNavigationItemSelectedListener(this);

        pb = (ProgressBar)findViewById(R.id.progressbar);
        pb.setMax(100);
        kodi = new Kodi(this);
        wv = (WebView)findViewById(R.id.webview);
        searchTorrent = (LinearLayout)findViewById(R.id.searchTorrent);
        searchHelper = new SearchHelper(this);
        searchResults = (ListView)searchTorrent.findViewById(R.id.searchresults);
        loadPage("home.html");
    }

    public void searchTorrents(){
        startRefreshing();
        if (searchHelper.isTorrentSearchInstalled()) {
            final List<SearchSite> sites = searchHelper.getAvailableSites();
            if ((sites!=null) && (sites.size()>0)) {
                wv.setVisibility(View.GONE);
                searchTorrent.setVisibility(View.VISIBLE);
                SearchView view = (SearchView) searchTorrent.findViewById(R.id.searchView);
                view.setIconifiedByDefault(false);
                view.requestFocus();
                view.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(final String query) {
                        startRefreshing();
                        Toast.makeText(Navigator.this, "Searching '" + query + "'...", Toast.LENGTH_SHORT).show();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                final ArrayList<SearchResult> allResults = new ArrayList<SearchResult>();
                                for (SearchSite site : sites) {
                                    final ArrayList<SearchResult> results = searchHelper.search(query, site, SearchHelper.SearchSortOrder.Combined);
                                    if ((results != null) && (results.size() > 0)) {
                                        for (SearchResult r : results){
                                            if (r.getTorrentUrl().trim().startsWith("magnet:"))
                                                allResults.add(r);
                                        }
                                    }
                                }
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        searchAdapter = new SearchResultAdapter(allResults,Navigator.this);
                                        searchResults.setAdapter(searchAdapter);
                                        searchResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                                Intent intent = new Intent(Navigator.this, IntentProcessor.class);
                                                intent.setAction(Intent.ACTION_VIEW);
                                                intent.setData(Uri.parse(searchAdapter.getItem(i).getTorrentUrl()));
                                                startActivity(intent);
                                            }
                                        });
                                        stopRefreshing();
                                    }
                                });
                            }
                        }).start();
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        return false;
                    }
                });
            }else
                Toast.makeText(Navigator.this, "No torrent sites available for search!", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(Navigator.this, "Transdroid Torrent Search is not installed!", Toast.LENGTH_SHORT).show();
        }
        stopRefreshing();
    }

    public void loadPage(final String page){
        wv.setVisibility(View.VISIBLE);
        searchTorrent.setVisibility(View.GONE);
        wv.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                pb.setProgress(progress);
            }
        });

        WebViewClient client = new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                stopRefreshing();
                pb.setProgress(0);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                try {
                    URL main = new URL("file:///android_res/raw/"+page);
                    URL uri = new URL(url);
                    if (main.getHost().equals(uri.getHost()))
                        return false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pb.setProgress(0);
                startRefreshing();
            }

        };
        wv.setWebViewClient(client);
        WebSettings webSettings = wv.getSettings();
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        wv.addJavascriptInterface(this, "PlayToKodi");
        wv.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        wv.setScrollbarFadingEnabled(false);
        wv.loadUrl("file:///android_res/raw/"+page);
    }

    @JavascriptInterface
    public void OpenMainMenu() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                drawer.openDrawer(Gravity.LEFT);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (wv.getVisibility() != View.VISIBLE){
                wv.setVisibility(View.VISIBLE);
            }
            else
            if (wv.canGoBack()) {
                wv.goBack();
            }else
                super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_remote) {
            Intent intent = new Intent(Navigator.this, RemoteControl.class);
            startActivityForResult(intent, 5001);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            loadPage("home.html");
        } else if (id == R.id.nav_remote) {
            Intent intent = new Intent(Navigator.this, RemoteControl.class);
            startActivityForResult(intent, 5001);
        } else if (id == R.id.nav_searchTorrents) {
            searchTorrents();
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(Navigator.this, SettingsActivity.class);
            startActivityForResult(intent, 5000);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void startRefreshing(){
        if (waitDialog!=null && waitDialog.isShowing())
            return;
        waitDialog = new Dialog(this);
        waitDialog.setCancelable(false);
        waitDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        RelativeLayout layout = (RelativeLayout)LayoutInflater.from(this).inflate(R.layout.dialog_loading, null);
        waitDialog.setContentView(layout);
        waitDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        waitDialog.show();
    }

    public void stopRefreshing(){
        if (waitDialog!=null)
            waitDialog.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        kodi = new Kodi(this);
    }
}
